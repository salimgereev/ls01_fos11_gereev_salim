
public class Feiertag extends Date {

	private String name;
	
	public Feiertag() {
				
				
	}
	
	public Feiertag(String name, int tag, int jahr, int monat){
		super(tag, jahr, monat);		
		this.name = name;
	}
	
		
	
	
	public String getName() {
		return name;
		
	}
	
	public void setName(String name) {
		this.name=name;

	}
	
	public String toString() {
		return "Datum : " + tag + "."  +  monat +"." + jahr + "  " + name;
		
	}
		
}
