
public class Kalenderdatumtest {

	public static void main(String[] args) {

		Date d1 = new Date();
		d1.setTag(01);
		d1.setMonat(02);
		d1.setJahr(2022);
		
		Feiertag f1 = new Feiertag();
		f1.setTag(24);
		f1.setMonat(12);
		f1.setJahr(2014);
		f1.setName("Heilig Abend");
		
		System.out.println(d1);
		System.out.println(f1);
	}

	
}
