
public class Date {

	protected int tag;
	protected int monat;
	protected int jahr;
	
	public Date() {
		this.jahr=1;
		this.tag=1;
		this.monat=1997;
					
	}
	
	public Date(int tag, int monat,int jahr){
		this.jahr=jahr;
		this.tag=tag;
		this.monat=monat;
		
		
		
	}
	
	
	
	public int getTag() {
		return tag;
	}
		
	public void setTag(int tag) {
		this.tag = tag;
	}
	
	public int getMonat() {
		return monat;
	}
	
	public void setMonat(int monat) {
		this.monat=monat;
		
	}
	
	
	public int getJahr() {
		return jahr;

	}
	
	public void setJahr(int jahr) {
		this.jahr=jahr;
	}
	
	
	
		public String toString() {
			return "Datum : " + tag + "."  +  monat +"." + jahr;	
	}
	
}
