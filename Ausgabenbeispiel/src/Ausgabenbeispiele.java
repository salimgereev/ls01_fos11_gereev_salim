
public class Ausgabenbeispiele {

	public static void main(String[] args) {
		
		// Kommzahl
		
		System.out.printf("Zahl: *%10.1f* %n", 12345.6789);
		
		// Ganzzhal
		
		System.out.printf("Zahl: *%-20d* %n", 123456789);
		
		// Zeichenkette
		System.out.printf("Wort: *%-20.4s","abcdef");
	}

}
