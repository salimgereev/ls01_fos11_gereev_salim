package de.oszimt.fos42.tagung;

public class Person {
	private String vorname;
	private String nachname;
	private String ort;
	protected int plz;
	private String strasse;
	private String telefonnummer;

	public Person() {

	}

	public Person(String vorname, String nachname, String ort, int plz, String strasse, String telefonnummer) {
		this.vorname = vorname;
		this.nachname = nachname;
		this.ort = ort;
		this.plz = plz;
		this.strasse = strasse;
		this.telefonnummer = telefonnummer;
		
	}

	public String getVorname() {
		return vorname;
	}

	public void setVorname(String vorname) {
		this.vorname = vorname;
	}

	public String getNachname() {
		return nachname;
	}

	public void setNachname(String nachname) {
		this.nachname = nachname;
	}

	public String getOrt() {
		return ort;
	}

	public void setOrt(String ort) {
		this.ort = ort;
	}

	public int getPlz() {
		return plz;
	}

	public void setPlz(int plz) {
		this.plz = plz;
	}

	public String getStrasse() {
		return strasse;
	}

	public void setStrasse(String strasse) {
		this.strasse = strasse;
	}

	public String getTelefonnummer() {
		return telefonnummer;
	}

	public void setTelefonnummer(String telefonnummer) {
		this.telefonnummer = telefonnummer;
	}
	
	public String toString()
	{
		return String.format("Person        | %20s | %20s |%n", this.getNachname(), this.getVorname());
	}
	
}
