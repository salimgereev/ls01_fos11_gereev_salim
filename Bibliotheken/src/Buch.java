
public class Buch {

	// attribute

	private String titel;
	private String autor;

	// Konstruktor
	public Buch() {
		this.titel = "unbekannt";
		this.autor = "unbekannt";

	}

	// Verwaltungsmehtoden

	public String getTitel() {
		return this.titel;

	}

	public String getAutor() {
		return this.autor;

	}

	public void setTitel(String titel) {
		this.titel = titel;
	}

	public void setAutor(String autor) {
		this.autor = autor;

	}
	
	public String toString(){
		 return ": Titel = "+this.titel + "; Autor = " + this.autor;


	}
	
	}

