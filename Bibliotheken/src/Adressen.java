
public class Adressen {
	private String strasse;
	private String plz;
	private String ort;

	public Adressen() {
		this.strasse = "unbekannt";
		this.plz = "unbekannt";
		this.ort = "unbekannt";
	}

	public String getStrasse() {
		return this.strasse;

	}

	public String getPlz() {
		return this.plz;
	}

	public String getOrt() {
		return this.ort;

	}

	public void setStrasse(String strasse) {
		this.strasse = strasse;
	}

	public void setPlz(String plz) {
		this.plz = plz;
	}

	public void setOrt(String ort) {
		this.ort = ort;

	}

	public String toString() {
		return "Adressen: Strasse = " + this.strasse + "; Plz = " + this.plz + "; ort = " + this.ort;
	}

}