
public class BuchTest {

	public static void main(String[] args) {
		
		Buch b1= new Buch();
		b1.setTitel("Koran");
		b1.setAutor("Gott");
		
		Buch b2= new Buch();
		b2.setTitel("Bibel");
		b2.setAutor("Apostel Johannes");
		
		Buch b3= new Buch();
		b3.setTitel("Bibel des Spaghettimonster");
		b3.setAutor("Bobby Henderson");
		
		Buch b4= new Buch();
		b4.setTitel("Harry Potter");
		b4.setAutor("Joanne Rowling");
		
		Buch b5= new Buch();
		b5.setTitel("Urfaust");
		b5.setAutor("Wolfgang von Goethe");
		
		System.out.println("Titel: "+b1.getTitel());
		System.out.println("Autor: "+b1.getAutor());
		
		System.out.println("Titel: "+b2.getTitel());
		System.out.println("Autor: "+b2.getAutor());
		
		System.out.println("Titel: "+b3.getTitel());
		System.out.println("Autor: "+b3.getAutor());
		
		System.out.println("Titel: "+b4.getTitel());
		System.out.println("Autor: "+b4.getAutor());
		
		System.out.println("Titel: "+b5.getTitel());
		System.out.println("Autor: "+b5.getAutor());
		
		
		//nur b ausgabe
		System.out.println(b2);
		System.out.println(b4 );
		{
			
		
		
			
		}
	}

}
