
public class Ausleihetest {

	public static void main(String[] args) {
		
		
		Ausleihe al1= new Ausleihe();
		al1.setAusleihnummer(11);
		
		
		Buch b1= new Buch();
		b1.setTitel("Koran");
		b1.setAutor("Gott");
		al1.setBuch(b1);
		
		Person p1= new Person();
		p1.setVorname("Ecio");
		p1.setNachname("Auditore");
		p1.setKundennummer("289173241247381283217");
		al1.setPerson(p1);
		
		Adressen a1= new Adressen();
		a1.setStrasse("Wartenberger str.22");
		a1.setPlz("11111");
		a1.setOrt("Berlin");
		p1.setAnschrift(a1);
		
		Ausleihe al2= new Ausleihe();
		al2.setAusleihnummer(22);
		
		Buch b2= new Buch();
		b2.setTitel("Bibel");
		b2.setAutor("Apostel Johannes");
		al2.setBuch(b2);
		
		Person p2= new Person();
		p2.setVorname("Master");
		p2.setNachname("Yi");
		p2.setKundennummer("289173126738128323217");
		al2.setPerson(p2);
		
		Adressen a2= new Adressen();
		a2.setStrasse("Hohenschonhauser str.20");
		a2.setPlz("22222");
		a2.setOrt("M�nchen");
		p2.setAnschrift(a2);
		
		
		System.out.println(al1);
		System.out.println(al2);
	}

}
