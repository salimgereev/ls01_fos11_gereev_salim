
public class Ausleihe {
	private int ausleihnummer;
	private Buch buch;
	private Person person;

	public Ausleihe() {
		this.ausleihnummer = 11;
		this.buch = new Buch();
		this.person = new Person();
	}

	public int getAusleihnummer() {
		return this.ausleihnummer;

	}

	public Buch getBuch() {
		return this.buch;
	}

	public Person getPerson() {
		return this.person;

	}

	public void setAusleihnummer(int ausleihnummer) {
		this.ausleihnummer = ausleihnummer;
	}

	public void setBuch(Buch b1) {
		this.buch = b1;
	}

	public void setPerson(Person p1) {
		this.person = p1;

	}

	public String toString() {
		return "Ausleihenummer: = " + this.ausleihnummer + "; Buch  " + this.buch + "; Person " + this.person;
	}

}