
public class Persontest {

	public static void main(String[] args) {
		Person p1= new Person();
		p1.setVorname("Ecio");
		p1.setNachname("Auditore");
		p1.setKundennummer("289173241247381283217");
		
		Adressen a1=new Adressen();
		a1.setStrasse("Wartenberger str.22");
		a1.setPlz("11111");
		a1.setOrt("Berlin");
		p1.setAnschrift(a1);
		
		Person p2= new Person();
		p2.setVorname("Master");
		p2.setNachname("Yi");
		p2.setKundennummer("289173126738128323217");
		
		Adressen a2= new Adressen();
		a2.setStrasse("Hohenschonhauser str.20");
		a2.setPlz("22222");
		a2.setOrt("M�nchen");
		p2.setAnschrift(a2);
		
		Person p3= new Person();
		p3.setVorname("Salim");
		p3.setNachname("Gereev");
		p3.setKundennummer("2891232367381283217");
		
		Adressen a3= new Adressen();
		a3.setStrasse("Leipziger str. 23");
		a3.setPlz("33333");
		a3.setOrt("Leipzig");
		p3.setAnschrift(a3);
		
		System.out.println(p1);
		System.out.println(p2);
		System.out.println(p3);
	}

	
}
