
public class Person {

	private String kundennummer;
	private String vorname;
	private String nachname;
	private Adressen anschrift;
	

	public Person() {
		this.kundennummer = "unbekannt";
		this.vorname = "unbekannt";
		this.nachname = "unbekannt";
		
		this.anschrift = new Adressen();
		
		
	}

	public String getKundennummer() {
		return this.kundennummer;

	}

	public String getVorname() {
		return this.vorname;
	}

	public String getNachname() {
		return this.nachname;

	}

	public void setKundennummer(String kundennummer) {
		this.kundennummer = kundennummer;
	}

	public void setVorname(String vorname) {
		this.vorname = vorname;
	}

	public void setNachname(String nachname) {
		this.nachname = nachname;

	}
	
	public Adressen getAnschrift() {
		return this.anschrift;
	}
	
	
	public void setAnschrift(Adressen anschrift) {
		this.anschrift=anschrift;
	
	}	
		
	public String toString() {
		return ": kundennummer = " + this.kundennummer + "; vorname = " + this.vorname + "; nachname = " + this.nachname +"; = " + this.anschrift;
	}

}
